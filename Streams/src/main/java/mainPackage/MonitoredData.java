package mainPackage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class MonitoredData {
	private String startTime;
	private String endTime;
	private String activity;
	
	public  static int calculate(String data)
	{
		String[] split = data.split(Pattern.quote("		"));
		String[] splitStart = split[0].split(Pattern.quote("-"));
		String[] splitEnd = split[1].split(Pattern.quote("-"));
		splitStart[splitStart.length - 1] = splitStart[splitStart.length -1].substring(0, 2);
		splitEnd[splitEnd.length - 1] = splitEnd[splitEnd.length -1].substring(0, 2);
		int year = 0;int month = 0;int day = 0;
		year = Integer.parseInt(splitEnd[0]) - Integer.parseInt(splitStart[0]);
		month = Integer.parseInt(splitEnd[1]) - Integer.parseInt(splitStart[1]);
		if(month < 0){
			month = 0;}
		day = Integer.parseInt(splitEnd[2]) - Integer.parseInt(splitStart[2]);
		if(day < 0){
			day = 1;}
		return day;
	}
	
	public MonitoredData(String startTime,String endTime,String activity)
	{
		this.startTime =  startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	public static int getCount(String entry){
		return 0;
	}
	
	public static String time(MonitoredData data)
	{
		String startTime = data.getStartTime();
		String endTime = data.getEndTime();
		startTime=startTime.replace(" ","T");
		endTime=endTime.replace(" ","T");
		DateTimeFormatter isoLocaDateTime = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		LocalDateTime date1 =  LocalDateTime.parse(startTime);
		LocalDateTime date2 = LocalDateTime.parse(endTime,isoLocaDateTime);
		long seconds = ChronoUnit.SECONDS.between(date1,date2);
		long hours = seconds/3600;
		long minutes = (seconds % 3600) / 60;
		seconds = seconds % 60;
		return new String(hours + ":" + minutes+":" + seconds);
	}
	
	public static long time2(MonitoredData data)
	{
		String startTime = data.getStartTime();
		String endTime = data.getEndTime();
		startTime=startTime.replace(" ","T");
		endTime=endTime.replace(" ","T");
		LocalDateTime date1 =  LocalDateTime.parse(startTime);
		LocalDateTime date2 = LocalDateTime.parse(endTime);
		long seconds = ChronoUnit.SECONDS.between(date1,date2);
		return seconds;
	}
	
	public static long time3(MonitoredData data)
	{
		String startTime = data.getStartTime();
		String endTime = data.getEndTime();
		startTime=startTime.replace(" ","T");
		endTime=endTime.replace(" ","T");
		LocalDateTime date1 =  LocalDateTime.parse(startTime);
		LocalDateTime date2 = LocalDateTime.parse(endTime);
		long seconds = ChronoUnit.SECONDS.between(date1,date2);
		if(seconds/60 < 5)
		{
			return seconds;
		}
		return 0;
	}
	public static boolean isValid(long date)
	{
		return (int)(date / 60) < 5;
	}
	public MonitoredData(){
		
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public String toString(){
		return String.format("%s %s %s", this.startTime,this.endTime,this.activity);
	}
	
	
}
