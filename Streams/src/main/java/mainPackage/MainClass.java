package mainPackage;

import java.awt.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MainClass {
	
	public static ArrayList<String> task1()
	{
		String path = "Activities.txt";
		ArrayList<String> list = new ArrayList<String>();
		System.out.println("Start Task1");
		try(Stream<String> stream = Files.lines(Paths.get(path))){
			list = (ArrayList<String>) stream
					.map(String::trim)
					.collect(Collectors.toList());
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		list.forEach(System.out::println);
		System.out.println("End Task 1");
		return list;
	}
	
	public static void task2(ArrayList<String> list)
	{
		System.out.println("Start Task2");
		Function<String, Integer> func = MonitoredData::calculate;
		
		int count = list.stream()
				.map(func)
				.reduce(0,Integer::sum);
		
		System.out.println("Number of days=" + (1+count));
		System.out.println("End Task 2");
	}
	
	public static Map<String,Long> task3(ArrayList<String> activities){
		System.out.println("Start Task 3");
		Map<String,Long> counts1 = activities.stream()
				.collect(Collectors.groupingBy(e->e,Collectors.counting()));
		
		
		counts1.entrySet().stream().forEach(System.out::println);
		System.out.println("End Task 3");
		return counts1;
	}
	
	public static void task4(ArrayList<String> activities2){
		System.out.println("Start Task 4");
		Map<String,Long> counts2 = activities2.stream()
				.collect(Collectors.groupingBy(e->e,Collectors.counting()));
		counts2.entrySet().stream().forEach(System.out::println);
		System.out.println("End Task 4");
	}
	
	public static void task5(ArrayList<MonitoredData> monitoredData)
	{
		System.out.println("Start Task 5");
		Function<MonitoredData, String> chrono = MonitoredData::time;
		Map<String,String> time = monitoredData.stream()
				.collect(Collectors.toMap(e->e.toString(),chrono));
		
		
		time.entrySet().stream().forEach(System.out::println);
		
		System.out.println("End Task 5");
	}
	public static void lastTask(ArrayList<MonitoredData> monitoredData,Map<String,Long> counts1)
	{
		System.out.println("Start Task 7");
		ArrayList<Long> values = (ArrayList<Long>) counts1.values().stream().map(x->(long)((90/(100 * 1.0) * x) -1)).collect(Collectors.toList());
		ArrayList<String> keySet = (ArrayList<String>) counts1.keySet().stream().collect(Collectors.toList());
		Map<String,Long> task7 = keySet.stream().collect(Collectors.toMap(k->k, v->values.get(keySet.indexOf(v))));task7.entrySet().stream().forEach(System.out::println);Map<String,Long> time2 = monitoredData.stream().filter(t->t.getActivity().equals("Sleeping")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));time2 =  time2.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time2.size() == task7.get("Sleeping")){time2.entrySet().stream().forEach(System.out::println);}Map<String,Long> time3 = monitoredData.stream().filter(t->t.getActivity().equals("Leaving")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));
		time3 =  time3.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));			if(time3.size() == task7.get("Leaving")){time3.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time4 = monitoredData.stream().filter(t->t.getActivity().equals("Toileting")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));
		time4 =  time4.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time4.size() == task7.get("Toileting")){time4.entrySet().stream().forEach(System.out::println);}Map<String,Long> time5 = monitoredData.stream().filter(t->t.getActivity().equals("Showering")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));	time5 =  time5.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time5.size() == task7.get("Showering")){time5.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time6 = monitoredData.stream().filter(t->t.getActivity().equals("Breakfast")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));time6 =  time6.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time6.size() == task7.get("Breakfast")){time6.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time7 = monitoredData.stream().filter(t->t.getActivity().equals("Lunch")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));
		time7 =  time7.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time7.size() == task7.get("Lunch")){time7.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time8 = monitoredData.stream().filter(t->t.getActivity().equals("Snack")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));time8 =  time8.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time8.size() == task7.get("Snack")){time8.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time9 = monitoredData.stream().filter(t->t.getActivity().equals("Spare_Time/TV")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));time9 =  time9.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time9.size() == task7.get("Spare_Time/TV")){time9.entrySet().stream().forEach(System.out::println);}
		Map<String,Long> time10= monitoredData.stream().filter(t->t.getActivity().equals("Grooming")).collect(Collectors.toMap(e->e.toString(),MonitoredData::time3));time10 =  time10.entrySet().stream().filter(t->t.getValue() != 0).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));if(time10.size() == task7.get("Grooming")){time10.entrySet().stream().forEach(System.out::println);}
		System.out.println("End Task 7");
	}
	public static void task6(ArrayList<MonitoredData> monitoredData)
	{
		System.out.println("Start Task 6");
		Map<String, Long> task6 = monitoredData.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingLong(MonitoredData::time2)));
		task6.entrySet().stream().forEach(System.out::println);
		System.out.println("End Task 6");
	}
	public static void main(String[] args) {
		ArrayList<String> list = task1();
		task2(list);
		ArrayList<String> activities = (ArrayList<String>) list.stream().map(x->x.substring(42)).collect(Collectors.toList());
		ArrayList<String> activities2 =(ArrayList<String>)list.stream().map(x->x.substring(0,10) + " " + x.substring(42)).collect(Collectors.toList());
		Map<String,Long> counts1 = task3(activities);
		task4(activities2);
		ArrayList<MonitoredData> monitoredData = (ArrayList<MonitoredData>) list.stream().map(x-> new MonitoredData(x.substring(0,19),x.substring(21, 40),x.substring(42))).collect(Collectors.toList());
		task5(monitoredData);
		task6(monitoredData);
		lastTask(monitoredData, counts1);
	}
}
